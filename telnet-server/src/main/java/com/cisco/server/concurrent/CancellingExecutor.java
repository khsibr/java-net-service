package com.cisco.server.concurrent;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class CancellingExecutor extends ThreadPoolExecutor {

	private static Logger logger = Logger.getLogger(CancellingExecutor.class);

	private static final int DEFAULT_AWAIT_COMPLETION = 2000; // 5 seconds

	public static CancellingExecutor getThreadPoolExecutor(int poolSize, int maxPoolSize, int maxRequestQueueSize,
			RejectedExecutionHandler rejectedExecutionHandler) {
		return new CancellingExecutor(poolSize, maxPoolSize, 0L, TimeUnit.MILLISECONDS,
				new LinkedBlockingQueue<Runnable>(maxRequestQueueSize), rejectedExecutionHandler);
	}

	public static CancellingExecutor newSingleThreadPoolExecutor() {
		return new CancellingExecutor(1, 2, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
	}

	public CancellingExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
	}

	public CancellingExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
	}

	public CancellingExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
	}

	public CancellingExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
	}

	public CancellingExecutor(ThreadPoolExecutor threadPoolExecutor) {
		super(threadPoolExecutor.getCorePoolSize(), threadPoolExecutor.getMaximumPoolSize(), threadPoolExecutor
				.getKeepAliveTime(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS, threadPoolExecutor.getQueue(),
				threadPoolExecutor.getThreadFactory(), threadPoolExecutor.getRejectedExecutionHandler());
	}

	@Override
	protected <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
		if (callable instanceof CancellableTask) {
			return ((CancellableTask<T>) callable).newTask();
		} else {
			return super.newTaskFor(callable);
		}

	}

	public void shutdownSafely(long timeout, TimeUnit unit) {
		try {
			logger.debug("Entered shutDownSafely");
			// Wait a while for existing tasks to terminate
			if (!awaitTermination(timeout, unit)) {
				logger.debug("Still have tasks");
				shutdownNow(); // Cancel currently
								// executing
								// tasks
				logger.debug("After shutdownNow");
				// Wait a while for tasks to respond to being cancelled
				if (!awaitTermination(DEFAULT_AWAIT_COMPLETION, unit)) {
					logger.error("Pool did not terminate");
				}
			}
		} catch (InterruptedException ie) {
			logger.debug("caught InterruptedException");
			// (Re-)Cancel if current thread also interrupted
			shutdownNow();
			// Preserve interrupt status
			Thread.currentThread().interrupt();
		}
	}

	/**
	 * Shutdown the thread pool. Stops news requests from being submitted. Waits
	 * for all the tasks already running to finish. Cancel running tasks after a
	 * timeout.
	 * 
	 * 
	 */
	public void shutDownSafely() {
		shutdownSafely(DEFAULT_AWAIT_COMPLETION, TimeUnit.MILLISECONDS);
	}
}
