package com.cisco.server.concurrent;

import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;

/**
 * 
 * @author ryadh
 * 
 */
public abstract class AbstractCancellableTask<T> implements CancellableTask<T> {

	private boolean isStopped = false;
	private final Object isStoppedLock = new Object();

	protected void setIsStopped(boolean isStopped) {
		synchronized (isStoppedLock) {
			this.isStopped = isStopped;
		}
	}

	public boolean isStopped() {
		synchronized (isStoppedLock) {
			return isStopped;
		}
	}

	@Override
	public RunnableFuture<T> newTask() {
		return new FutureTask<T>(this) {
			@SuppressWarnings("finally")
			@Override
			public boolean cancel(boolean mayInterruptIfRunning) {
				try {
					AbstractCancellableTask.this.cancel();
				} finally {
					return super.cancel(mayInterruptIfRunning);
				}
			}
		};
	}

	protected void cleanupResourcesSafely() {
		synchronized (isStoppedLock) {
			cleanupResources();
			isStopped = true;
		}
	}

	abstract protected void cleanupResources();
}