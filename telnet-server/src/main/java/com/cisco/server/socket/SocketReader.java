package com.cisco.server.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

import org.apache.log4j.Logger;

public class SocketReader {
	private static  Logger logger = Logger.getLogger(SocketReader.class);
	private final Socket connection;
	private final BufferedReader bufferedReader;
	
	// Should not be used, just for testing
	protected SocketReader() {
		this.connection = null;
		this.bufferedReader = null;
	}
	
	public SocketReader(Socket connection) {
		this.connection = connection;
		this.bufferedReader = initBufferedReader();
		
	}
	

	// Blocking read from the socket input stream
	// returns null if the connection is closed by the client
	public String readNextCommand() throws IOException{
		String line =  bufferedReader.readLine();
		logger.debug("request:"+line);
		return line;
	}
	
	//Very ugly to throw Exception during constructing.. re-throw Runtime
	private BufferedReader initBufferedReader()  {
		InputStream inputStream;
		try {
			inputStream = connection.getInputStream();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return new BufferedReader(new InputStreamReader(inputStream));

	}

	public void cleanup() throws IOException{
		bufferedReader.close();
	}
}
