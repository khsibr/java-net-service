package com.cisco.server.socket;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;

import org.apache.log4j.Logger;

public class SocketWriter {

	private static final String RESPONSE_HEADER = "";
	private static final String DEFAULT_PROMPT = ">";
	private static Logger logger = Logger.getLogger(SocketWriter.class);

	private final Socket connection;
	private final BufferedWriter bufferedWriter;

	public SocketWriter(Socket connection) {
		this.connection = connection;
		bufferedWriter = initBufferedWriter();

	}

	public void writeResponseAndPrompt(String response) throws IOException {
		logger.debug("response:" + response);
		response = RESPONSE_HEADER + response + System.getProperty("line.separator") + DEFAULT_PROMPT;
		bufferedWriter.write(response);
		bufferedWriter.flush();
	}

	public void writelnResponse(String response) throws IOException {
		logger.debug("response:" + response);
		response = RESPONSE_HEADER + response + System.getProperty("line.separator") + DEFAULT_PROMPT;
		bufferedWriter.write(response);
		bufferedWriter.flush();
	}

	// Very ugly to throw Exception during constructing.. re-throw
	// RuntimeException
	private BufferedWriter initBufferedWriter() {
		OutputStream outputStream;
		try {
			outputStream = connection.getOutputStream();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		return new BufferedWriter(new OutputStreamWriter(outputStream));
	}

	public void cleanup() throws IOException {
		bufferedWriter.close();
	}

}
