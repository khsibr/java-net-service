package com.cisco.server;

import java.net.Socket;


public interface TaskProcessorFactory{

	TaskProcessor getClientConnectionTaskProcessor(Socket connection);
}
