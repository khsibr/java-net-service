package com.cisco.server;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.cisco.server.concurrent.CancellingExecutor;

public class ServerStarter implements LifeCycle {

	private static Logger logger = Logger.getLogger(ServerStarter.class);
	static {
		initLogger();
	}
	// used only by one thread
	private CancellingExecutor serverSingleThreadPool;
	// used only by one thread
	private Future<?> serverServiceTask;
	// used only by one thread
	private final TaskProcessorFactory taskProcessorFactory;
	
	
	
	public ServerStarter(TaskProcessorFactory taskProcessorFactory) {
		this.taskProcessorFactory = taskProcessorFactory;
	}

	@Override
	public void start() {
		logger.info("Starting the server ..");
		serverSingleThreadPool = CancellingExecutor.newSingleThreadPoolExecutor();
		ServerConfiguration serverConfiguration = new ServerConfiguration();
		logger.debug(serverConfiguration);
		serverServiceTask = serverSingleThreadPool.submit(new ServerService(serverConfiguration,taskProcessorFactory));
	}

	static private void initLogger() {
		Properties properties = new Properties();
		java.net.URL url = ServerConfiguration.class.getClassLoader().getResource("conf/log4j.properties");
		try {
			properties.load(url.openStream());
			PropertyConfigurator.configure(properties);
		} catch (IOException e) {
			logger.error("Could not initialise log4j", e);
		}
	}

	@Override
	public void stop() {
		logger.info("Stopping the server ..");
		serverServiceTask.cancel(true); // Should interrupt the server thread
		serverSingleThreadPool.shutDownSafely();
	}


}
