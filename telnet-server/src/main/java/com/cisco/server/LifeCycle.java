package com.cisco.server;

public interface LifeCycle {

	void start();

	void stop();

}
