package com.cisco.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.cisco.server.concurrent.AbstractCancellableTask;
import com.cisco.server.concurrent.CancellingExecutor;

/**
 * Telnet server. Typical Use: TelnetServer server = new TelnetServer();
 * This class is thread safe in the sense that if the start and stop is called by different threads,
 * it will behave consistently
 * 
 * @author ryadh
 * 
 */
public class ServerService extends AbstractCancellableTask<Void> {

	private static final int INIT_TIMEOUT = 2000;
	private static Logger logger = Logger.getLogger(ServerService.class);
	// Configuration
	private final ServerConfiguration serverConfiguration;

	// Not final because it is better to initialize the socket and the pool when the
	// server starts
	private CancellingExecutor requestProcessorThreadPool;
	private ServerSocket serverSocket;

	private final List<Future<?>> submittedTasks = new ArrayList<Future<?>>();

	private final CountDownLatch initLatch = new CountDownLatch(1);
	private final TaskProcessorFactory taskProcessorFactory;

	public ServerService(ServerConfiguration serverConfiguration, TaskProcessorFactory taskProcessorFactory) {
		this.serverConfiguration = serverConfiguration;
		this.taskProcessorFactory = taskProcessorFactory;
	}

	/**
	 * Starts the server
	 * 
	 * @throws IllegalStateException
	 *             if the server is already started
	 */
	@Override
	public Void call() {
		logger.info("Starting the server thread ..");
		try {
			init();
			logger.info("Server init completed ..");
		} catch (IOException e) {
			// Fatal error, stop
			logger.error(e);
			return null;
		}

		while (!isStopped()) {
			Socket clientSocket;
			// Blocks until new client connection is requested
			try {
				logger.debug("Starting the server socket..");
				clientSocket = serverSocket.accept();
			} catch (IOException e) {
				handleServerSocketIOExecption(e);
				break;
			}

			logger.info("accepting connection from:" + clientSocket.getRemoteSocketAddress());
			// A Persistent connection/client runs in a separate thread from the
			// thread pool
			// The connection termination is requested by the client using ^]
			submitRequest(clientSocket);
		}
		cleanupResourcesSafely();
		return null;

	}

	private void submitRequest(Socket clientSocket) {
		try {
			TaskProcessor task = taskProcessorFactory.getClientConnectionTaskProcessor(clientSocket);
			submittedTasks.add(requestProcessorThreadPool.submit(task));
		} catch (RejectedExecutionException e) {
			logger.info("ThreadPool queue limit reached. Current limit:" + serverConfiguration.getMaxRequestQueueSize());
		}
	}

	private void handleServerSocketIOExecption(IOException e) {
		if (isStopped()) {
			logger.debug("Server socket stopped ..");
		} else {
			logger.error(e);
		}
	}

	private void init() throws IOException {
		if (initLatch.getCount() == 0) { // server already started
			throw new IllegalStateException("Server already started");
		}
		// Start the server socket
		serverSocket = new ServerSocket(serverConfiguration.getServerPort());
		initThreadPool();
		setIsStopped(false);
		initLatch.countDown();
	}

	private void initThreadPool() {
		// CallerRunsPolicy is used to slow down clients connections by using
		// the caller thread to execute them
		requestProcessorThreadPool = CancellingExecutor.getThreadPoolExecutor(serverConfiguration.getPoolSize(),
				serverConfiguration.getMaxPoolSize(), serverConfiguration.getMaxRequestQueueSize(),
				serverConfiguration.getRejectedExecutionHandler());
	}

	/**
	 * Stops the server
	 * 
	 * @throws IllegalStateException
	 *             if the server is already stopped
	 */
	@Override
	public void cancel() {
		logger.info("Stopping the server thread ..");
		if (!waitForServerInit()) {
			throw new IllegalStateException("Server not yet started");
		}

		for (Future<?> processorTask : submittedTasks) {
			processorTask.cancel(true);

		}

		cleanupResourcesSafely();
	}

	private boolean waitForServerInit() {
		try {
			if (initLatch.getCount() > 0) {
				logger.info("Waiting for the server init ..");
				return initLatch.await(INIT_TIMEOUT, TimeUnit.MILLISECONDS);

			}
		} catch (InterruptedException e) {
			// Interrupted before the initialisation finishes, exist
			Thread.currentThread().interrupt();
		}
		return true;
	}

	@Override
	protected void cleanupResources() {
		cleanupThreadPool();
		cleanupServerSocket();
	}

	private void cleanupServerSocket() {
		if (serverSocket == null) {
			return;
		}
		try {
			serverSocket.close();
		} catch (IOException e) {
			logger.error(e);
		} finally {
			serverSocket = null;
		}

	}

	private void cleanupThreadPool() {
		if (requestProcessorThreadPool == null) {
			return;
		}

		try {
			requestProcessorThreadPool.shutDownSafely();
		} finally {
			requestProcessorThreadPool = null;
		}

	}

}