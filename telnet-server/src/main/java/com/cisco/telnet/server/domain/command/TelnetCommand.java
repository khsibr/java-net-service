package com.cisco.telnet.server.domain.command;

import com.cisco.telnet.server.domain.request.CommandRequest;
import com.cisco.telnet.server.domain.response.CommandResponse;
import com.cisco.telnet.server.task.SessionContext;

//Command Pattern
public interface TelnetCommand {

	// Command don't throw exceptions. It builds the response
	// containing the requested data or the error messages
	CommandResponse execute(CommandRequest request, SessionContext sessionContext);

}
