package com.cisco.telnet.server.domain.response;

public enum ResponseStatusEnum {

	ERROR(1), OK(0);

	final int status;

	private ResponseStatusEnum(int status) {
		this.status = status;
	}

}
