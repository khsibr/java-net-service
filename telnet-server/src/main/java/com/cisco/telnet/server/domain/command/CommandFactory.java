package com.cisco.telnet.server.domain.command;

import java.util.HashMap;
import java.util.Map;

import com.cisco.telnet.server.domain.command.shell.CdCommand;
import com.cisco.telnet.server.domain.command.shell.LsCommand;
import com.cisco.telnet.server.domain.command.shell.MkdirCommand;
import com.cisco.telnet.server.domain.command.shell.PwdCommand;
import com.cisco.telnet.server.domain.command.shell.operation.ShellOperation;
import com.cisco.telnet.server.domain.command.shell.operation.ShellOperationParser;

//Thread safe
public class CommandFactory {

	public static final String PWD = "pwd";
	public static final String MKDIR = "mkdir";
	public static final String LS = "ls";
	public static final String CD = "cd";
	// static initialisation: thread safe
	// HashMap not thread safe, but effectively immutable in this case (Read
	// only access)
	static final Map<String, TelnetCommand> commandsMap = new HashMap<String, TelnetCommand>();
	static {
		commandsMap.put(CD, new CdCommand());
		commandsMap.put(LS, new LsCommand());
		commandsMap.put(MKDIR, new MkdirCommand());
		commandsMap.put(PWD, new PwdCommand());

	}

	private final ShellOperationParser shellOperationParser;

	public CommandFactory() {
		shellOperationParser = new ShellOperationParser();
	}

	public TelnetCommand getCommand(String command) {
		ShellOperation shellOperation = shellOperationParser.getShellOperation(command);
		return commandsMap.get(shellOperation.getName());
	}

}
