package com.cisco.telnet.server.domain.response;

public interface CommandResponse {

	String getResultAsString();

	ResponseStatusEnum getResponseStatus();

}
