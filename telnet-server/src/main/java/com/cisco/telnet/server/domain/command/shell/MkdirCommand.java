package com.cisco.telnet.server.domain.command.shell;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.cisco.telnet.server.domain.command.shell.operation.ShellOperation;
import com.cisco.telnet.server.domain.response.CommandResponse;
import com.cisco.telnet.server.domain.response.StringCommandResponse;
import com.cisco.telnet.server.task.SessionContext;

public class MkdirCommand extends ShellCommand {

	private static Logger logger = Logger.getLogger(MkdirCommand.class);

	private static final String FAILURE_MESSAGE = "Unable to create the directory:";
	private static final String MISSING_OPERAND_MESSAGE = "missing operand";
	private static String SUCCESS_MESSAGE = "Successfully created the directory:";

	@Override
	protected CommandResponse executeOperation(ShellOperation shellOperation, SessionContext sessionContext) {

		String newDirectory;
		if (shellOperation.isEmptyParameters()) {
			return buildErrorResponse(shellOperation, MISSING_OPERAND_MESSAGE);
		} else {
			newDirectory = shellOperation.getFirstParameter();
		}

		File newDirectoryFile = buildAbsolutePath(sessionContext.getCurrentDirectory(), newDirectory);

		String message;
		try {
			FileUtils.forceMkdir(newDirectoryFile);
			message = SUCCESS_MESSAGE + newDirectory;
		} catch (IOException e) {
			logger.error(e);
			return buildErrorResponse(shellOperation, FAILURE_MESSAGE + newDirectory);
		}

		return new StringCommandResponse(message);
	}

}
