package com.cisco.telnet.server.domain.request;


public class StringCommandRequest implements CommandRequest{
	private final String request;
	
	public StringCommandRequest(String request) {
		this.request = request;
	}

	public String getRequestAsString() {
		return request;
	}
}
