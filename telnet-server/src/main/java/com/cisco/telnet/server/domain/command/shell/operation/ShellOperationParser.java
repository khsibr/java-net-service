package com.cisco.telnet.server.domain.command.shell.operation;

import java.util.ArrayList;
import java.util.List;

// Thread safe
public class ShellOperationParser {

	public ShellOperation getShellOperation(String command) {
		String[] parts = command.split(" ");
		List<String> parameters = new ArrayList<String>();
		if (parts.length > 1) {
			for (int i = 1; i < parts.length; i++) {
				parameters.add(parts[i]);

			}
		}
		return new ShellOperation(parts[0], parameters);
	}

}
