package com.cisco.telnet.server.task;

import com.cisco.server.session.TaskProcessorSession;

// Not thread safe
public class SessionContext implements TaskProcessorSession{

	private String currentDirectory;
	private static final String USER_DIR = "user.dir";

	public SessionContext() {
		currentDirectory = System.getProperty(USER_DIR);
	}

	public String getCurrentDirectory() {
		return currentDirectory;
	}

	public void setCurrentDirectory(String currentDirectory) {
		this.currentDirectory = currentDirectory;
	}

}
