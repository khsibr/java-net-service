package com.cisco.telnet.server.domain.command.shell;

import java.io.File;

import com.cisco.telnet.server.domain.command.shell.operation.ShellOperation;
import com.cisco.telnet.server.domain.response.CommandResponse;
import com.cisco.telnet.server.domain.response.StringCommandResponse;
import com.cisco.telnet.server.task.SessionContext;

public class CdCommand extends ShellCommand {

	private static final String PARENT_PATH = "..";
	private static final String NO_SUCH_FILE_OR_DIRECTORY_MESSAGE = " No such file or directory";

	@Override
	protected CommandResponse executeOperation(ShellOperation shellOperation, SessionContext sessionContext) {
		String destinationDirectory;
		// Default to user home cd
		if (shellOperation.isEmptyParameters()) {
			destinationDirectory = System.getProperty(ShellConstants.USER_HOME);
		} else {
			destinationDirectory = shellOperation.getFirstParameter();

		}

		String currentDirectory = sessionContext.getCurrentDirectory();

		if (destinationDirectory.equals(PARENT_PATH)) {
			destinationDirectory = new File(currentDirectory).getParent();
		}

		File destinationDirectoryFile = buildAbsolutePath(currentDirectory, destinationDirectory);

		if (destinationDirectoryFile.exists()) {
			sessionContext.setCurrentDirectory(destinationDirectoryFile.getAbsolutePath());
			// return the new dir message
			return new StringCommandResponse("New directory:" + destinationDirectory);
		} else {
			// Error
			return buildErrorResponse(shellOperation, destinationDirectory + NO_SUCH_FILE_OR_DIRECTORY_MESSAGE);
		}
	}

}
