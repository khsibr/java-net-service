package com.cisco.telnet.server.domain.command.shell;

import java.io.File;

import com.cisco.telnet.server.domain.command.TelnetCommand;
import com.cisco.telnet.server.domain.command.shell.operation.ShellOperation;
import com.cisco.telnet.server.domain.command.shell.operation.ShellOperationParser;
import com.cisco.telnet.server.domain.request.CommandRequest;
import com.cisco.telnet.server.domain.response.CommandResponse;
import com.cisco.telnet.server.domain.response.ResponseStatusEnum;
import com.cisco.telnet.server.domain.response.StringCommandResponse;
import com.cisco.telnet.server.task.SessionContext;

public abstract class ShellCommand implements TelnetCommand {

	private final ShellOperationParser shellOperationParser;

	public ShellCommand() {
		shellOperationParser = new ShellOperationParser();
	}

	@Override
	public CommandResponse execute(CommandRequest commandRequest, SessionContext sessionContext) {
		System.out.println(sessionContext.getCurrentDirectory());
		ShellOperation shellOperation = shellOperationParser.getShellOperation(commandRequest.getRequestAsString());
		return executeOperation(shellOperation, sessionContext);
	}

	abstract protected CommandResponse executeOperation(ShellOperation shellOperation, SessionContext sessionContext);

	protected CommandResponse buildErrorResponse(ShellOperation shellOperation, String errorMessage) {
		return new StringCommandResponse(shellOperation.getName() + ": " + errorMessage, ResponseStatusEnum.ERROR);
	}

	protected String buildFileAbsolutePath(String fileName, String directoryName) {
		return directoryName + System.getProperty("file.separator") + fileName;
	}

	protected File buildAbsolutePath(String currentDirectory, String requestDirectory) {
		File requestDirectoryFile = new File(requestDirectory);

		// If already absolute, return it
		if (requestDirectoryFile.isAbsolute()) {
			return requestDirectoryFile;
		}
		// Else build the absolute path, check if it exits, then return it
		String requestDirectoryAbsolute = buildFileAbsolutePath(requestDirectory, currentDirectory);
		File requestDirectoryAbsoluteFile = new File(requestDirectoryAbsolute);

		return requestDirectoryAbsoluteFile;
	}
}
