package com.cisco.telnet.server.domain.command.shell;

public interface ShellConstants {

	static final String CURRENT_DIR = ".";
	static final String USER_DIR = "user.dir";
	static final String USER_HOME = "user.home";
	static final String LINE_SEPARATOR = System.getProperty("line.separator");
	static final String FILE = "f";
	static final String DIRECTORY = "d";
	static final String TAB = "\t";
}
