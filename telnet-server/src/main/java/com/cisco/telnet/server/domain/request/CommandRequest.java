package com.cisco.telnet.server.domain.request;

public interface CommandRequest {

	String getRequestAsString();
	
}
