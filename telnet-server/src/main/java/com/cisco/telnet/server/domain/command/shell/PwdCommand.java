package com.cisco.telnet.server.domain.command.shell;

import com.cisco.telnet.server.domain.command.shell.operation.ShellOperation;
import com.cisco.telnet.server.domain.response.CommandResponse;
import com.cisco.telnet.server.domain.response.StringCommandResponse;
import com.cisco.telnet.server.task.SessionContext;

public class PwdCommand extends ShellCommand {

	@Override
	protected CommandResponse executeOperation(ShellOperation shellOperation, SessionContext sessionContext) {
		return new StringCommandResponse(sessionContext.getCurrentDirectory());
	}

}
