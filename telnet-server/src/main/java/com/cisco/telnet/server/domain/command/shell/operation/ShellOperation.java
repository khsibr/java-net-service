package com.cisco.telnet.server.domain.command.shell.operation;

import java.util.ArrayList;
import java.util.List;

//Immutable
public final class ShellOperation {

	private final String name;
	private final List<String> parameters;

	public ShellOperation(String name, List<String> parameters) {
		this.name = name;
		this.parameters = new ArrayList<String>(parameters);
	}

	public ShellOperation(String name) {
		this.name = name;
		parameters = new ArrayList<String>();
	}

	public String getName() {
		return name;
	}

	public List<String> getParameters() {
		return new ArrayList<String>(parameters);
	}

	public boolean isEmptyParameters() {
		return parameters.size() == 0;
	}

	public String getFirstParameter() {
		return parameters.get(0);
	}

}
