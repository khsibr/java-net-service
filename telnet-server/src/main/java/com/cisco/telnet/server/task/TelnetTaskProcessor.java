package com.cisco.telnet.server.task;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

import org.apache.log4j.Logger;

import com.cisco.server.concurrent.AbstractCancellableTask;
import com.cisco.server.session.StatefulTaskProcessor;
import com.cisco.server.socket.SocketReader;
import com.cisco.server.socket.SocketWriter;
import com.cisco.telnet.server.domain.command.CommandFactory;
import com.cisco.telnet.server.domain.command.TelnetCommand;
import com.cisco.telnet.server.domain.request.CommandRequest;
import com.cisco.telnet.server.domain.request.StringCommandRequest;
import com.cisco.telnet.server.domain.response.CommandResponse;

/**
 * A Runnable, a persistent client collection. Listens to client commands,
 * execute them and write the response. Detects client disconnected and do the
 * cleanup.
 * 
 * This class is running in one thread from the pool. The connection, the reader
 * and the writer are confined to this thread which guarantees thread safety.
 * 
 * @author ryadh
 * 
 */
public class TelnetTaskProcessor extends AbstractCancellableTask<Void> implements StatefulTaskProcessor{

	private static Logger logger = Logger.getLogger(TelnetTaskProcessor.class);

	private static final String CONNECTION_TIMEOUT_MESSAGE = "Connection timeout ";
	private static final String CONNECTION_CLOSED_FROM_MESSAGE = "Connection closed from ";
	private static final String UNEXPECTED_SERVER_ERROR_MESSAGE = "Unexpected server error";
	private static final String INVALID_COMMAND_MESSAGE = "invalid command: ";

	private static final int DEFAULT_CONNECTION_IDLE_TIMEOUT = 60000; // 60
																		// seconds
	private final int connectionIdleTimeoutInMs;

	private final CommandFactory commandFactory;

	private final Socket connection;
	private final SocketReader socketReader;
	private final SocketWriter socketWriter;
	// used only by one thread
	private final SessionContext sessionContext = new SessionContext();

	public TelnetTaskProcessor(Socket connection, SocketReader socketReader, SocketWriter commandWriter,
			CommandFactory commandFactory, int connectionIdleTimeout) {
		this.connection = connection;
		this.socketReader = socketReader;
		this.socketWriter = commandWriter;
		this.commandFactory = commandFactory;
		this.connectionIdleTimeoutInMs = connectionIdleTimeout;
	}

	public TelnetTaskProcessor(Socket connection) {
		this.connection = connection;
		this.socketReader = new SocketReader(connection);
		this.socketWriter = new SocketWriter(connection);
		this.commandFactory = new CommandFactory();
		this.connectionIdleTimeoutInMs = DEFAULT_CONNECTION_IDLE_TIMEOUT;
	}

	@Override
	public Void call() {
		setConnectionTimeout();

		writeWelcomeMessage();

		// The connection is controlled interruption by the client (Escape
		// character is '^]'.)
		// It is detected with a null read command.
		while (true) {
			String response;
			try {
				String command = null;
				// Read the command. Blocks until a connection timeout
				try {
					command = socketReader.readNextCommand();
				} catch (SocketTimeoutException e) {
					handleTimeoutException();
					break;
				} catch (IOException e) {
					handleReadIOException(e);
					break;
				}

				// The communication is closed by the client
				if (command == null) {
					logger.info(CONNECTION_CLOSED_FROM_MESSAGE + getRemoteHost());
					break;
				}

				// Get command executer
				TelnetCommand telnetCommand = commandFactory.getCommand(command);
				// Invalid command
				if (telnetCommand == null) {
					response = INVALID_COMMAND_MESSAGE + command;
				}
				// Execute the command
				else {
					response = executeCommand(command, telnetCommand);
				}

			} catch (IOException e) {
				logger.error(e);
				response = UNEXPECTED_SERVER_ERROR_MESSAGE;
			}
			// Write the response
			try {
				socketWriter.writeResponseAndPrompt(response);
			} catch (IOException e) {
				logger.error(e);
				// Communication issues.. May be the socket is closed.
				break;
			}

		}
		cleanupResourcesSafely();
		return null;

	}

	private void handleTimeoutException() throws IOException {
		logger.debug(getTimeoutMessage());
		socketWriter.writeResponseAndPrompt(getTimeoutMessage());
	}

	private void handleReadIOException(IOException e) {
		if (isStopped()) {
			logger.debug("Client " + getRemoteHost() + " connection exited normally");
		} else {
			logger.error(e);
		}
	}

	private void writeWelcomeMessage() {
		try {
			socketWriter.writeResponseAndPrompt("Connected to server " + connection.getLocalAddress());
		} catch (IOException e) {
			logger.error(e);
		}

	}

	protected String getTimeoutMessage() {
		return CONNECTION_TIMEOUT_MESSAGE + connectionIdleTimeoutInMs / 1000 + " seconds";
	}

	private String executeCommand(String command, TelnetCommand telnetCommand) {
		String response;
		CommandRequest commandRequest = new StringCommandRequest(command);
		CommandResponse commandResponse = telnetCommand.execute(commandRequest, sessionContext);
		response = commandResponse.getResultAsString();
		return response;
	}

	private void setConnectionTimeout() {
		try {
			connection.setSoTimeout(connectionIdleTimeoutInMs);
		} catch (SocketException e) {
			logger.warn("Cannot set the timeout on the connection socket, disabled", e);
		}
	}

	@Override
	protected void cleanupResources() {
		// Cleanup, always writer before reader other the write data may be
		// lost
		try {
			socketWriter.cleanup();
			socketReader.cleanup();
			connection.close();
		} catch (IOException e) {
			logger.error(e);
		}

	}

	private String getRemoteHost() {
		return connection.getRemoteSocketAddress().toString();
	}

	@Override
	public void cancel() {
		logger.info("Cancelling task - client " + getRemoteHost());
		try {
			if (!isStopped()) {
				socketWriter.writeResponseAndPrompt("Server stopping. Goodbye!");
			}
		} catch (IOException e) {
			logger.error(e);
		}
		cleanupResourcesSafely();
	}
}
