package com.cisco.telnet.server.task;

import java.net.Socket;

import com.cisco.server.TaskProcessor;
import com.cisco.server.TaskProcessorFactory;

public class TelnetTaskProcessorFactory implements TaskProcessorFactory{

	@Override
	public TaskProcessor getClientConnectionTaskProcessor(Socket connection) {
		return new TelnetTaskProcessor(connection);
	}

}
