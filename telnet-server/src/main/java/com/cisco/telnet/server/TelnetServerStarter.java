package com.cisco.telnet.server;

import com.cisco.server.LifeCycle;
import com.cisco.server.ServerStarter;
import com.cisco.telnet.server.task.TelnetTaskProcessorFactory;

public class TelnetServerStarter implements LifeCycle {

	private final TelnetTaskProcessorFactory taskProcessorFactory = new TelnetTaskProcessorFactory();
	private final ServerStarter serverStarter = new ServerStarter(taskProcessorFactory);


	@Override
	public void start() {
		serverStarter.start();
	}

	@Override
	public void stop() {
		serverStarter.stop();
	}

	public static void main(String[] args) {
		TelnetServerStarter telnetServer = new TelnetServerStarter();
		telnetServer.start();
	}

}
