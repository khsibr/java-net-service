package com.cisco.telnet.server.domain.command.shell;

import static com.cisco.telnet.server.domain.command.shell.ShellConstants.DIRECTORY;
import static com.cisco.telnet.server.domain.command.shell.ShellConstants.FILE;
import static com.cisco.telnet.server.domain.command.shell.ShellConstants.LINE_SEPARATOR;
import static com.cisco.telnet.server.domain.command.shell.ShellConstants.TAB;

import java.io.File;

import com.cisco.telnet.server.domain.command.shell.operation.ShellOperation;
import com.cisco.telnet.server.domain.response.CommandResponse;
import com.cisco.telnet.server.domain.response.StringCommandResponse;
import com.cisco.telnet.server.task.SessionContext;

public class LsCommand extends ShellCommand {

	public static final String FOLDER_TO_LIST_NOT_FOUND_MESSAGE = "File to list not found:";

	@Override
	protected CommandResponse executeOperation(ShellOperation shellOperation, SessionContext sessionContext) {
		String currentDirectory = sessionContext.getCurrentDirectory();

		// If the parameters are empty, default to the current directory
		String toListDirectory;
		if (shellOperation.isEmptyParameters()) {
			toListDirectory = currentDirectory;
		} else {
			toListDirectory = shellOperation.getFirstParameter();
		}
		File toListDirectoryFile = buildAbsolutePath(currentDirectory, toListDirectory);
		if (!toListDirectoryFile.exists()) {
			return new StringCommandResponse(FOLDER_TO_LIST_NOT_FOUND_MESSAGE + toListDirectory);
		}

		// Build toListDirectory absolute path

		// List the files
		String[] content = toListDirectoryFile.list();
		String lsResult = "";
		for (int i = 0; i < content.length; i++) {
			lsResult += buildFileInfo(content[i], toListDirectory);
			if (i < content.length - 1) {
				lsResult += LINE_SEPARATOR;
			}
		}
		return new StringCommandResponse(lsResult);
	}

	private String buildFileInfo(String fileName, String directoryName) {
		String filePath = buildFileAbsolutePath(fileName, directoryName);
		File file = new File(filePath);
		String dirIndcator;
		if (file.isDirectory()) {
			dirIndcator = DIRECTORY;
		} else {
			dirIndcator = FILE;
		}

		return dirIndcator + TAB + fileName;
	}

}
