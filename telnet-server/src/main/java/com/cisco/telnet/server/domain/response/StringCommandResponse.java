package com.cisco.telnet.server.domain.response;

//Immutable
public final class StringCommandResponse implements CommandResponse {

	private final String result;
	private final ResponseStatusEnum responseStatus;

	public StringCommandResponse(String result, ResponseStatusEnum responseStatus) {
		this.result = result;
		this.responseStatus = responseStatus;
	}

	// Default status is OK
	public StringCommandResponse(String result) {
		this.result = result;
		responseStatus = ResponseStatusEnum.OK;
	}

	@Override
	public String getResultAsString() {
		return result;
	}

	@Override
	public ResponseStatusEnum getResponseStatus() {
		return responseStatus;
	}

}
