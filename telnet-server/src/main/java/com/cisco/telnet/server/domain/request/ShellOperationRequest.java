package com.cisco.telnet.server.domain.request;

import com.cisco.telnet.server.domain.command.shell.operation.ShellOperation;
import com.cisco.telnet.server.domain.command.shell.operation.ShellOperationParser;


public class ShellOperationRequest{

	private final String request;
	private final ShellOperationParser shellOperationParser;


	public ShellOperationRequest(CommandRequest commandRequest) {
		this(commandRequest.getRequestAsString());
	}

	public ShellOperationRequest(String request) {
		this.request = request;
		shellOperationParser = new ShellOperationParser();
	}

	public String getRequestAsString() {
		return request;
	}

	public ShellOperation getShellOperation(){
		return shellOperationParser.getShellOperation(request);
	}
}
