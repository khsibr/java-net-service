package com.cisco.telnet.server.domain.command.shell;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.cisco.telnet.server.domain.command.CommandFactory;
import com.cisco.telnet.server.domain.command.shell.operation.ShellOperation;
import com.cisco.telnet.server.domain.response.CommandResponse;
import com.cisco.telnet.server.domain.response.ResponseStatusEnum;
import com.cisco.telnet.server.task.SessionContext;

@RunWith(MockitoJUnitRunner.class)
public class MkdirCommandTest {

	private static final String TMP_DIR = "tmpDir";
	private final MkdirCommand mkdirCommand = new MkdirCommand();
	private ShellOperation shellOperation;
	private CommandResponse response;
	@Mock
	private SessionContext sessionContext;

	@Test
	public void testExecute() throws IOException {
		shellOperation = new ShellOperation(CommandFactory.MKDIR, Arrays.asList(TMP_DIR));
		String currentUserDir = System.getProperty("user.dir");
		Mockito.when(sessionContext.getCurrentDirectory()).thenReturn(currentUserDir);

		response = mkdirCommand.executeOperation(shellOperation, sessionContext);

		File tmpDir = new File(TMP_DIR);
		assertEquals(ResponseStatusEnum.OK, response.getResponseStatus());
		assertTrue(tmpDir.exists());

		FileUtils.deleteDirectory(tmpDir);
	}
}
