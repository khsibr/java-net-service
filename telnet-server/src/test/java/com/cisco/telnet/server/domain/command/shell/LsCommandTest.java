package com.cisco.telnet.server.domain.command.shell;

import static com.cisco.telnet.server.domain.command.shell.ShellConstants.DIRECTORY;
import static com.cisco.telnet.server.domain.command.shell.ShellConstants.FILE;
import static com.cisco.telnet.server.domain.command.shell.ShellConstants.TAB;
import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.cisco.telnet.server.domain.command.CommandFactory;
import com.cisco.telnet.server.domain.command.shell.operation.ShellOperation;
import com.cisco.telnet.server.domain.response.CommandResponse;
import com.cisco.telnet.server.domain.response.ResponseStatusEnum;
import com.cisco.telnet.server.task.SessionContext;

@RunWith(MockitoJUnitRunner.class)
public class LsCommandTest {

	private static final String TMP_FILE2 = "tmpFile2";
	private static final String TMP_FILE1 = "tmpFile1";
	private static final String TMP_DIR = "tmpDir";
	private static final String TMP_DIR1 = "tmpDir1";
	private final LsCommand lsCommand = new LsCommand();
	private CommandResponse response;
	private ShellOperation shellOperation;
	@Mock
	private SessionContext sessionContext;

	@Test
	public void testExecute() throws IOException {
		File tmpDir = new File(TMP_DIR);
		FileUtils.forceMkdir(tmpDir);
		FileUtils.touch(new File(TMP_DIR + "/" + TMP_FILE1));
		FileUtils.touch(new File(TMP_DIR + "/" + TMP_FILE2));
		FileUtils.forceMkdir(new File(TMP_DIR + "/" + TMP_DIR1));
		String currentUserDir = System.getProperty("user.dir");
		Mockito.when(sessionContext.getCurrentDirectory()).thenReturn(currentUserDir);
		shellOperation = new ShellOperation(CommandFactory.LS, Arrays.asList(TMP_DIR));

		response = lsCommand.executeOperation(shellOperation, sessionContext);
		assertEquals(ResponseStatusEnum.OK, response.getResponseStatus());
		Assert.assertTrue(response.getResultAsString().contains(FILE + TAB + TMP_FILE1));
		Assert.assertTrue(response.getResultAsString().contains(FILE + TAB + TMP_FILE2));
		Assert.assertTrue(response.getResultAsString().contains(DIRECTORY + TAB + TMP_DIR1));

		FileUtils.deleteDirectory(tmpDir);
	}
}
