package com.cisco.telnet.server.domain.command.shell;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.cisco.telnet.server.domain.command.CommandFactory;
import com.cisco.telnet.server.domain.command.shell.operation.ShellOperation;
import com.cisco.telnet.server.domain.response.CommandResponse;
import com.cisco.telnet.server.domain.response.ResponseStatusEnum;
import com.cisco.telnet.server.task.SessionContext;

@RunWith(MockitoJUnitRunner.class)
public class PwdCommandTest {

	private static final String CURRENT_DIR = "currentDir";
	private final PwdCommand pwdCommand = new PwdCommand();
	private ShellOperation shellOperation;
	private CommandResponse response;
	@Mock
	private SessionContext sessionContext;

	@Test
	public void testExecute() throws IOException {
		shellOperation = new ShellOperation(CommandFactory.PWD);
		Mockito.when(sessionContext.getCurrentDirectory()).thenReturn(CURRENT_DIR);
		response = pwdCommand.executeOperation(shellOperation, sessionContext);

		assertEquals(sessionContext.getCurrentDirectory(), response.getResultAsString());
		assertEquals(ResponseStatusEnum.OK, response.getResponseStatus());
	}
}
