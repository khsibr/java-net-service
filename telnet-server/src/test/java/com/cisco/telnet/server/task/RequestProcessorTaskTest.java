package com.cisco.telnet.server.task;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.runners.MockitoJUnitRunner;

import com.cisco.server.socket.SocketReader;
import com.cisco.server.socket.SocketWriter;
import com.cisco.telnet.server.domain.command.CommandFactory;
import com.cisco.telnet.server.domain.command.shell.PwdCommand;
import com.cisco.telnet.server.domain.request.CommandRequest;
import com.cisco.telnet.server.domain.response.CommandResponse;
import com.cisco.telnet.server.domain.response.StringCommandResponse;

@RunWith(MockitoJUnitRunner.class)
public class RequestProcessorTaskTest {

	private static final String TMP_DIR = "/tmp";
	private static final int IDLE_TIMEOUT = 1;
	private static final String PWD = "pwd";
	private TelnetTaskProcessor requestProcessorTask;
	@Mock
	private Socket connection;
	@Mock
	private SocketReader commandReader;
	@Mock
	private SocketWriter commandWriter;
	@Mock
	private CommandFactory commandFactory;
	@Mock
	private PwdCommand pwdCommand;
	@Mock
	private SessionContext sessionContext;

	private CommandRequest anyRequest;

	@Before
	public void setUp() throws Exception {
		requestProcessorTask = new TelnetTaskProcessor(connection, commandReader, commandWriter, commandFactory,
				IDLE_TIMEOUT);
	}

	@Test
	public void testcallTimeout() throws IOException {
		when(commandReader.readNextCommand()).thenThrow(new SocketTimeoutException());

		requestProcessorTask.call();
		// Reaching this point proves that the call exited

		Mockito.verify(commandWriter).writeResponseAndPrompt(requestProcessorTask.getTimeoutMessage());
		Mockito.verify(commandReader).cleanup();
		Mockito.verify(commandWriter).cleanup();
		Mockito.verify(connection).close();
	}

	@Test
	public void testcallClientCloses() throws IOException {
		when(commandReader.readNextCommand()).thenReturn(null);
		when(connection.getRemoteSocketAddress()).thenReturn(new InetSocketAddress(1111));

		requestProcessorTask.call();
		// Reaching this point proves that the call exited

		Mockito.verify(commandWriter, VerificationModeFactory.times(0)).writeResponseAndPrompt(
				requestProcessorTask.getTimeoutMessage());
		Mockito.verify(commandReader).cleanup();
		Mockito.verify(commandWriter).cleanup();
		Mockito.verify(connection).close();
	}

	@Test
	public void testcall() throws IOException {
		requestProcessorTask = new TelnetTaskProcessor(connection, new TestCommandReader(PWD), commandWriter,
				commandFactory, IDLE_TIMEOUT);
		when(connection.getRemoteSocketAddress()).thenReturn(new InetSocketAddress(1111));

		when(commandFactory.getCommand(PWD)).thenReturn(pwdCommand);
		anyRequest = Matchers.any();
		sessionContext = Matchers.any();
		CommandResponse anyResponse = new StringCommandResponse(TMP_DIR);
		when(pwdCommand.execute(anyRequest, sessionContext)).thenReturn(anyResponse);

		requestProcessorTask.call();
		// Should exist because the second call to the test reader will return
		// null

		Mockito.verify(commandWriter).writeResponseAndPrompt(TMP_DIR);
		Mockito.verify(commandWriter).cleanup();
		Mockito.verify(connection).close();
	}

}

class TestCommandReader extends SocketReader {

	int counter = -1;
	private final String command;

	public TestCommandReader(String command) {
		this.command = command;
	}

	@Override
	public String readNextCommand() throws IOException {
		counter++;
		if (counter == 0) {
			return command;
		} else {
			return null;
		}
	}

	@Override
	public void cleanup() throws IOException {
	}
}
