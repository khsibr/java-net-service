package com.cisco.telnet.server.domain.command.shell;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.cisco.telnet.server.domain.command.CommandFactory;
import com.cisco.telnet.server.domain.command.shell.operation.ShellOperation;
import com.cisco.telnet.server.domain.response.CommandResponse;
import com.cisco.telnet.server.domain.response.ResponseStatusEnum;
import com.cisco.telnet.server.task.SessionContext;

@RunWith(MockitoJUnitRunner.class)
public class CdCommandTest {

	private static final String TMP_DIR = "tmpDir";
	private final CdCommand cdCommand = new CdCommand();
	private ShellOperation shellOperation;
	private CommandResponse response;
	@Mock
	private SessionContext sessionContext;

	@Test
	public void testExecute() throws IOException {
		File tmpDir = new File(TMP_DIR);
		FileUtils.forceMkdir(tmpDir);
		String currentUserDir = System.getProperty("user.dir");
		Mockito.when(sessionContext.getCurrentDirectory()).thenReturn(currentUserDir);
		shellOperation = new ShellOperation(CommandFactory.CD, Arrays.asList(TMP_DIR));

		response = cdCommand.executeOperation(shellOperation, sessionContext);

		assertEquals(ResponseStatusEnum.OK, response.getResponseStatus());
		// assertEquals(new File(TMP_DIR).getAbsoluteFile(),
		// sessionContext.getCurrentDirectory());
		Mockito.verify(sessionContext).setCurrentDirectory(new File(TMP_DIR).getAbsoluteFile().toString());

		FileUtils.deleteDirectory(tmpDir);
	}

	@Test
	public void testExecuteNotFound() throws IOException {
		shellOperation = new ShellOperation(CommandFactory.CD, Arrays.asList(TMP_DIR));
		String currentUserDir = System.getProperty("user.dir");
		Mockito.when(sessionContext.getCurrentDirectory()).thenReturn(currentUserDir);
		CommandResponse commandResponse = cdCommand.executeOperation(shellOperation, sessionContext);

		assertEquals(ResponseStatusEnum.ERROR, commandResponse.getResponseStatus());
		Mockito.verify(sessionContext, Mockito.never()).setCurrentDirectory(
				new File(TMP_DIR).getAbsoluteFile().toString());
	}
}
