package com.cisco.telnet.server.domain.command;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.cisco.telnet.server.domain.command.shell.CdCommand;
import com.cisco.telnet.server.domain.command.shell.LsCommand;
import com.cisco.telnet.server.domain.command.shell.MkdirCommand;
import com.cisco.telnet.server.domain.command.shell.PwdCommand;

public class CommandFactoryTest {

	private CommandFactory commandFactory = new CommandFactory();
	
	@Test
	public void testGetCommand() {
		assertEquals(CdCommand.class, commandFactory.getCommand("cd").getClass());
		assertEquals(LsCommand.class, commandFactory.getCommand("ls").getClass());
		assertEquals(MkdirCommand.class, commandFactory.getCommand("mkdir").getClass());
		assertEquals(PwdCommand.class, commandFactory.getCommand("pwd").getClass());
		assertEquals(null, commandFactory.getCommand("xxxx"));
		
	}

}
