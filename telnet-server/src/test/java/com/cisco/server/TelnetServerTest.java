package com.cisco.server;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.cisco.server.ServerConfiguration;
import com.cisco.server.ServerStarter;
import com.cisco.server.ServerService;
import com.cisco.telnet.server.task.TelnetTaskProcessorFactory;

public class TelnetServerTest {

	private static final int NTRIALS = 10;
	private static final int SLEEP_DEFAULT = 2000;
	private final ServerConfiguration serverConfiguration = new ServerConfiguration();
	private final TelnetTaskProcessorFactory processorTaskFactory = new TelnetTaskProcessorFactory();
	@Test
	public void testStartcancel() throws InterruptedException {
		final ServerStarter server = new ServerStarter(processorTaskFactory);
		server.start();
		sleep();
		server.stop();
		assertTrue(true); // Asserts that the server shutdown exists

	}

	@Test
	public void testStartConcurrentStartcancel() throws InterruptedException {
		for (int i = 1; i < NTRIALS; i++) {
			final ServerService server = new ServerService(serverConfiguration, processorTaskFactory);
			new Thread(new Runnable() {

				@Override
				public void run() {
					server.call();

				}
			}).start();
			sleep();
			server.cancel();
			assertTrue(server.isStopped());
		}

	}

	private void sleep() throws InterruptedException {
		Thread.sleep(SLEEP_DEFAULT);
	}

	@Test
	public void testStartExceptionCleanup() throws InterruptedException {
		final ServerService server = new ServerService(serverConfiguration, processorTaskFactory);
		boolean gotException = false;
		// start in 1st thread
		new Thread(new Runnable() {

			@Override
			public void run() {
				server.call();

			}
		}).start();
		sleep();
		// start in 2nd thread
		try {
			server.call();

		} catch (IllegalStateException e) {
			gotException = true;
		}
		assertTrue(gotException);// should throw an exception
		server.cancel();
		assertTrue(server.isStopped());

	}

	@Test(expected = IllegalStateException.class)
	public void testStopcall() {
		ServerService server = new ServerService(serverConfiguration, processorTaskFactory);
		server.cancel();

	}

	@Test(expected = IllegalStateException.class)
	public void testStopState() {
		ServerService server = new ServerService(serverConfiguration, processorTaskFactory);
		server.cancel();

	}

}
